public class Sphere extends Circle
{

	public Sphere()
	{
		super();
	}

	public Sphere(double r, double thickness)
	{
		super(r, thickness);
	}

	public String toString()
	{
		return /* "Here is the variables that is used in this sphere: \nRadius - " + getRadius() + "\nVolume - " + findMetalVolume() +
		"\nSurfacearea - " +findSurfaceArea() + */ "\nVekten til kula er: " + findWeight();
	}

	public double findVolume()
	{
		return ((4 * Math.PI * Math.pow(getRadius(),3))/3);
	}

	public double findInnerVolume()
	{
		return ((4 * Math.PI * Math.pow((getRadius()-getThickness()),3))/3);
	}

	public double findMetalVolume()
	{
		return findVolume() - findInnerVolume();
	}

	public double findSurfaceArea()
	{
		return 4 * Math.PI * Math.pow(getRadius(),2);
	}

	public double findWeight()
	{
		return getDensity() * findMetalVolume();
	}
}