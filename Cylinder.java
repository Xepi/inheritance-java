public class Cylinder extends Circle
{
	private double length;

	public Cylinder()
	{
		super();
		length = 1.0;
	}

	public Cylinder(double r,double l, double thickness)
	{
		super(r, thickness);
		length = l;
	}

	public double getLength()
	{
		return length;
	}

	public void setLength(double l)
	{
		length = (l>=0) ? l : 0;
	}

	public String toString()
	{
		return /*"Here is the variables that is used in this cyliner: \nRadius - " + getRadius() + " \nLength - " + getLength() +
		"\nVolume - " + findMetalVolume() + "\nSurfacearea - " + findSurfaceArea() +*/ "\nVekten til sylinderen er: " + findWeight();
	}

	public double findVolume()
	{
		return findArea() * length;
	}

	public double findInnerVolume()
	{
		return Math.pow((getRadius() - getThickness()),2) * Math.PI * (length - (getThickness() * 2));
	}

	public double findMetalVolume()
	{
		return findVolume() - findInnerVolume();
	}

	public double findSurfaceArea()
	{
		return (findCircumference() * length) + (2 * findArea());
	}

	public double findWeight()
	{
		return getDensity() * findMetalVolume();
	}
}