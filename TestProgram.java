import java.util.Scanner;
public class TestProgram
{
	static Scanner input = new Scanner(System.in);

	public static void main(String[]args)
	{
		int i;
		System.out.println("Hva slags objekt vil du m�le vekten av?\nSylinder = 1\nKule = 2\nBoks = 3\nAlle tre = Alle andre tall");
		i = input.nextInt();
		if(i==1)
		{
			System.out.println("Hvilken metaltype vil du ha?\nJern = 1\nAliminium = 2\nKobber = 3\nTinn = 4");
			int metaltype = input.nextInt();
			System.out.println("Hva skal radiusen til sylinderen v�re?");
			double radiuscylinder = input.nextDouble();
			System.out.println("Hva skal lengden til sylinderen v�re?");
			double lengthcylinder = input.nextDouble();
			System.out.println("Hva skal tykkelsen p� objektet v�re?");
			double thickness = input.nextDouble();

			Cylinder cylinder = new Cylinder(radiuscylinder, lengthcylinder, thickness);
			cylinder.setDensity(metaltype);
			System.out.println(cylinder.toString() + "\n");

		}
		else if(i==2)
		{
			System.out.println("Hvilken metaltype vil du ha?\nJern = 1\nAliminium = 2\nKobber = 3\nTinn = 4");
			int metaltype = input.nextInt();
			System.out.println("Hva skal radiusen til kula v�re?");
			double radiussphere = input.nextDouble();
			System.out.println("Hva skal tykkelsen p� objektet v�re?");
			double thickness = input.nextDouble();

			Sphere sphere = new Sphere(radiussphere, thickness);
			sphere.setDensity(metaltype);
			System.out.println(sphere.toString() + "\n");
		}
		else if(i==3)
		{
			System.out.println("Hvilken metaltype vil du ha?\nJern = 1\nAliminium = 2\nKobber = 3\nTinn = 4");
			int metaltype = input.nextInt();
			System.out.println("Hva skal lengden til boksen v�re?");
			double lengthbox = input.nextDouble();
			System.out.println("Hva skal bredden til boksen v�re?");
			double widthbox = input.nextDouble();
			System.out.println("Hva skal h�yden til boksen v�re?");
			double heightbox = input.nextDouble();
			System.out.println("Hva skal tykkelsen p� objektet v�re?");
			double thickness = input.nextDouble();

			Box box = new Box(heightbox, lengthbox, widthbox, thickness);
			box.setDensity(metaltype);
			System.out.println(box.toString() + "\n");
		}
		else
		{
			System.out.println("Hvilken metaltype vil du ha?\nJern = 1\nAliminium = 2\nKobber = 3\nTinn = 4");
			int metaltype = input.nextInt();
			System.out.println("Hva skal radiusen til sylinderen v�re?");
			double radiuscylinder = input.nextDouble();
			System.out.println("Hva skal lengden til sylinderen v�re?");
			double lengthcylinder = input.nextDouble();
			System.out.println("Hva skal radiusen til kula v�re?");
			double radiussphere = input.nextDouble();
			System.out.println("Hva skal lengden til boksen v�re?");
			double lengthbox = input.nextDouble();
			System.out.println("Hva skal bredden til boksen v�re?");
			double widthbox = input.nextDouble();
			System.out.println("Hva skal h�yden til boksen v�re?");
			double heightbox = input.nextDouble();
			System.out.println("Hva skal tykkelsen p� objektet v�re?");
			double thickness = input.nextDouble();

			Cylinder cylinder = new Cylinder(radiuscylinder, lengthcylinder, thickness);
			cylinder.setDensity(metaltype);
			System.out.println(cylinder.toString() + "\n");

			Sphere sphere = new Sphere(radiussphere, thickness);
			sphere.setDensity(metaltype);
			System.out.println(sphere.toString() + "\n");

			Box box = new Box(heightbox, lengthbox, widthbox, thickness);
			box.setDensity(metaltype);
			System.out.println(box.toString() + "\n");
		}
	}
}