public abstract class Rectangle extends GeometricObject
{
private double length, width;

	public Rectangle()
	{
		super();
		length = 1.0;
		width = 1.0;
	}

	public Rectangle(double l,double w, double thickness)
	{
		super(thickness);
		length = l;
		width = w;
	}

	public double getLength()
	{
		return length;
	}

	public double getWidth()
	{
		return width;
	}

	public void setLength(double l)
	{
		length = (l >= 0) ? l : 0;
	}

	public void setWidth(double w)
	{
		width = (w >= 0) ? w : 0;
	}

	public double findArea()
	{
		return length * width;
	}

	public double findCircumference()
	{
		return (2 * length) + (2 * width);
	}
}