public abstract class Circle extends GeometricObject
{
	private double radius;

	public Circle()
	{
		super();
		radius = 1.0;
	}

	public Circle(double r, double thickness)
	{
		super(thickness);
		radius = r;
	}

	public double getRadius()
	{
		return radius;
	}

	public void setRadius(double r)
	{
		radius = (r >= 0) ? r : 0;
	}

	public double findArea()
	{
		return Math.pow(radius, 2) * Math.PI;
	}

	public double findCircumference()
	{
		return 2 * Math.PI * radius;
	}
}