public class Box extends Rectangle
{
	private double height;

	public Box()
	{
		super();
		height = 1.0;
	}

	public Box(double h, double l, double w, double thickness)
	{
		super(l,w,thickness);
		height = h;
	}

	public double getHeight()
	{
		return height;
	}

	public void setHeight(double h)
	{
		height = (h>=0) ? h : 0;
	}

	public String toString()
	{
		return /* "Here is the variables that is used in this box: \nLength - " + getLength() +
		" \nWidth - " + getWidth() + "\nHeight - " + height + "\nVolume - " + findMetalVolume() + "\nSurfacearea - " +
		findSurfaceArea() + */ "\nVekten til boksen er: " + findWeight();
	}


	public double findVolume()
	{
		return findArea() * height;
	}

	public double findInnerVolume()
	{
		return (getLength() - (getThickness() * 2)) * (getWidth() - (getThickness() * 2)) * (height - (getThickness() * 2));
	}

	public double findMetalVolume()
	{
		return findVolume() - findInnerVolume();
	}

	public double findSurfaceArea()
	{
		return (findArea()*2) + (getLength() * height * 2) + (getWidth() * height * 2);
	}

	public double findWeight()
	{
		return getDensity() * findMetalVolume();
	}
}