public abstract class GeometricObject
{

	public abstract double findMetalVolume();

	public abstract double findArea();

	public abstract double findCircumference();

	public abstract double findSurfaceArea();

	public abstract double findWeight();



	private double thickness;

	public GeometricObject()
	{}

	public GeometricObject(double thickness)
	{
		this.thickness = thickness;
	}

	public void setThickness(double thickness)
	{
		this.thickness = thickness;
	}

	public double getThickness()
	{
		return thickness;
	}

	private double density;
	int i;
	public void setDensity(int i)
	{
		switch(i)
		{
			case 1: density = 7.8; //Jern
					break;
			case 2: density = 2.7; //Aliminium
					break;
			case 3: density = 8.9; //Kobber
					break;
			case 4: density = 7.3; //Tinn
					break;
		}


	}

	public double getDensity()
	{
		return density;
	}


}

